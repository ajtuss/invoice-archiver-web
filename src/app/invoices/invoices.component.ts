import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatSort} from '@angular/material';
import {Invoice, InvoicesDataSource} from './invoices-datasource';
import {SelectionModel} from '@angular/cdk/collections';
import {InvoicesService} from './invoices.service';

@Component({
  selector: 'app-invoices',
  templateUrl: './invoices.component.html',
  styleUrls: ['./invoices.component.css'],
})
export class InvoicesComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource: InvoicesDataSource;
  selection = new SelectionModel<Invoice>(false, []);

  displayedColumns = [
    'select',
    'id',
    'invoiceNumber',
    'companyShortName',
    'contractorShortName',
    'issueDate',
    'dueDate',
    'netTotalAmount',
    'taxTotalAmount',
    'grossTotalAmount'
  ];


  constructor(private invoiceService: InvoicesService) {
  }

  ngOnInit() {
    this.dataSource = new InvoicesDataSource(this.invoiceService, this.paginator, this.sort);
    this.dataSource.loadInvoices();
  }

  deleteSelectedInvoice() {
    const id = this.selection.selected[0].id;
    this.invoiceService.delete(id).subscribe(
      () => {
      },
      error1 => console.log(error1),
      () => {
        alert('Invoice is deleted.');
        this.dataSource.loadInvoices();
      }
    );
  }
}
