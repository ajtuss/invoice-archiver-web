import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, ValidationErrors, Validators} from '@angular/forms';
import {InvoicesService} from '../invoices.service';
import {Invoice} from '../invoices-datasource';
import {combineLatest, Observable} from 'rxjs';
import {Company} from '../../company/company-datasource';
import {CompanyService} from '../../company/company.service';
import {debounceTime, filter, map, startWith, switchMap} from 'rxjs/operators';
import {Contractor} from '../../contractors/contractors-datasource';
import {Router} from '@angular/router';

@Component({
  selector: 'app-add-invoice',
  templateUrl: './add-invoice.component.html',
  styleUrls: ['./add-invoice.component.css'],
})
export class AddInvoiceComponent implements OnInit {
  invoiceForm = this.fb.group({
    invoiceNumber: [null, Validators.required],
    issueDate: [null, Validators.required],
    dueDate: [null, Validators.required],
    netTotalAmount: [null, Validators.required],
    taxTotalAmount: [null, Validators.required],
    grossTotalAmount: [null, Validators.required],
    companyId: [null, Validators.required],
    contractorId: [null, Validators.required]
  });

  companySelect = new FormControl();
  contractorSelect = new FormControl();
  filteredCompanies: Observable<Company[]>;
  filteredContractors: Observable<Contractor[]>;


  constructor(private invoicesService: InvoicesService,
              private companyService: CompanyService,
              private fb: FormBuilder,
              private route: Router) {
  }

  onSubmit() {
    this.invoiceForm.patchValue({
      'companyId': this.companySelect.value.id,
      'contractorId': this.contractorSelect.value.id
    });
    this.invoicesService.add(<Invoice>this.invoiceForm.value).subscribe(
      value => this.route.navigate(['invoices', value.id]),
      error => {
        error.error.errors.forEach(e => this.setError(e.field, {[e.defaultMessage]: true})
        );
      },
      () => alert('Invoice is added'));
  }

  setError(fieldName: string, errors?: ValidationErrors) {
    this.invoiceForm.controls[fieldName].setErrors(errors);
  }

  ngOnInit(): void {
    this.filteredCompanies = this.companySelect.valueChanges
      .pipe(
        startWith(''),
        debounceTime(200),
        switchMap(value => this.companyService.getAll(0, 10, value))
      );

    const selectedCompanyId = this.companySelect.valueChanges.pipe(
      filter(value => value.id),
      map(value => <number>value.id));

    const contractorsFilter = this.contractorSelect.valueChanges
      .pipe(
        startWith(''),
        debounceTime(200),
        map((value: string) => value)
      );
    this.filteredContractors = combineLatest(selectedCompanyId, contractorsFilter)
      .pipe(
        switchMap(value => this.companyService.getAllContractorsByCompanyId(value[0], 10, value[1])),
      );
  }


  displayCompanyName(company?: Company): string | undefined {
    return company ? company.shortName : undefined;
  }

  displayContractorName(contractor?: Contractor): string | undefined {
    return contractor ? contractor.shortName : undefined;
  }
}
