import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, ValidationErrors, Validators} from '@angular/forms';
import {combineLatest, Observable} from 'rxjs';
import {Company} from '../../company/company-datasource';
import {Contractor} from '../../contractors/contractors-datasource';
import {InvoicesService} from '../invoices.service';
import {CompanyService} from '../../company/company.service';
import {Invoice} from '../invoices-datasource';
import {debounceTime, filter, map, startWith, switchMap} from 'rxjs/operators';
import {ActivatedRoute} from '@angular/router';
import {ContractorService} from '../../contractors/contractor.service';

@Component({
  selector: 'app-edit-invoice',
  templateUrl: './edit-invoice.component.html',
  styleUrls: ['./edit-invoice.component.css'],
})
export class EditInvoiceComponent implements OnInit {
  invoiceForm = this.fb.group({
    id: [],
    invoiceNumber: [null, Validators.required],
    issueDate: [null, Validators.required],
    dueDate: [null, Validators.required],
    netTotalAmount: [null, Validators.required],
    taxTotalAmount: [null, Validators.required],
    grossTotalAmount: [null, Validators.required],
    companyId: [null, Validators.required],
    companyShortName: [],
    contractorId: [null, Validators.required],
    contractorShortName: []
  });
  companySelect = new FormControl();
  contractorSelect = new FormControl();
  filteredCompanies: Observable<Company[]>;
  filteredContractors: Observable<Contractor[]>;


  constructor(private invoicesService: InvoicesService,
              private companyService: CompanyService,
              private contractorService: ContractorService,
              private fb: FormBuilder,
              private route: ActivatedRoute) {
  }

  onSubmit() {
    this.invoiceForm.patchValue({
      'companyId': this.companySelect.value.id,
      'contractorId': this.contractorSelect.value.id
    });
    this.invoicesService.update(<Invoice>this.invoiceForm.value).subscribe(
      () => {},
      error => {
        error.error.errors.forEach(e => this.setError(e.field, {[e.defaultMessage]: true})
        );
      },
      () => alert('Invoice is saved'));
  }

  setError(fieldName: string, errors?: ValidationErrors) {
    this.invoiceForm.controls[fieldName].setErrors(errors);
  }

  ngOnInit(): void {
    this.route.params.subscribe(value => {
      this.invoicesService.getById(+value['id']).subscribe(
        value1 => {
          this.invoiceForm.setValue(value1);
          this.companyService.getById(value1.companyId).subscribe(
            company => this.companySelect.setValue(company)
          );
          this.contractorService.getById(value1.contractorId).subscribe(
            contractor => this.contractorSelect.setValue(contractor)
          );
        }
      );
    });

    this.filteredCompanies = this.companySelect.valueChanges
      .pipe(
        startWith(''),
        debounceTime(200),
        switchMap(value => this.companyService.getAll(0, 10, value))
      );

    const selectedCompanyId = this.companySelect.valueChanges.pipe(
      filter(value => value.id),
      map(value => <number>value.id));

    const contractorsFilter = this.contractorSelect.valueChanges
      .pipe(
        startWith(''),
        debounceTime(200),
        map((value: string) => value)
      );
    this.filteredContractors = combineLatest(selectedCompanyId, contractorsFilter)
      .pipe(
        switchMap(value => this.companyService.getAllContractorsByCompanyId(value[0], 10, value[1])),
      );
  }


  displayCompanyName(company?: Company): string | undefined {
    return company ? company.shortName : undefined;
  }

  displayContractorName(contractor?: Contractor): string | undefined {
    return contractor ? contractor.shortName : undefined;
  }
}
