import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient, HttpParams} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {Invoice} from './invoices-datasource';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class InvoicesService {

  private url: string = environment.apiURL;

  constructor(private http: HttpClient) {
  }

  getAll(pageNumber: number, pageSize: number): Observable<Invoice[]> {
    return this.http.get(this.url + 'invoices', {
      params: new HttpParams()
        .set('size', pageSize.toString())
        .set('page', pageNumber.toString())
    }).pipe(map((res: Invoice[]) => res));
  }

  add(invoice: Invoice): Observable<Invoice> {
    return this.http.post(this.url + 'invoices', invoice)
      .pipe(map((res: Invoice) => res));
  }

  update(invoice: Invoice): Observable<Invoice> {
    return this.http.put(this.url + 'invoices', invoice)
      .pipe(map((res: Invoice) => res));
  }

  getById(id: number): Observable<Invoice> {
    return this.http.get(this.url + 'invoices/' + id)
      .pipe(map((res: Invoice) => res));
  }

  delete(id: number): Observable<void> {
    return this.http.delete(this.url + 'invoices/' + id)
      .pipe(map(() => {
      }));
  }

}
