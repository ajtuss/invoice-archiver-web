import {CollectionViewer, DataSource} from '@angular/cdk/collections';
import {MatPaginator, MatSort} from '@angular/material';
import {BehaviorSubject, Observable, of} from 'rxjs';
import {InvoicesService} from './invoices.service';
import {catchError, finalize} from 'rxjs/operators';

export interface Invoice {
  id: number;
  invoiceNumber: string;
  issueDate: string;
  dueDate: string;
  netTotalAmount: number;
  taxTotalAmount: number;
  grossTotalAmount: number;
  companyId: number;
  companyShortName: string;
  contractorId: number;
  contractorShortName: string;
}

export class InvoicesDataSource extends DataSource<Invoice> {

  private invoiceSubject = new BehaviorSubject<Invoice[]>([]);
  private loadingSubject = new BehaviorSubject<boolean>(false);

  constructor(private invoicesService: InvoicesService, private paginator: MatPaginator, private sort: MatSort) {
    super();
  }


  connect(collectionViewer: CollectionViewer): Observable<Invoice[]> {
    return this.invoiceSubject.asObservable();
  }

  disconnect() {
    this.invoiceSubject.complete();
    this.loadingSubject.complete();
  }

  loadInvoices(sortDirection = 'asc', pageIndex = 0, pageSize = 10) {

    this.loadingSubject.next(true);

    this.invoicesService.getAll(pageIndex, pageSize).pipe(
      catchError(() => of([])),
      finalize(() => this.loadingSubject.next(false))
    )
      .subscribe(invoices => this.invoiceSubject.next(invoices));
  }
}


