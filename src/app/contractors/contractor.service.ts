import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {Contractor} from './contractors-datasource';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ContractorService {

  private url: string = environment.apiURL;

  constructor(private http: HttpClient) {
  }

  getAll(pageNumber: number, pageSize: number, filter = ''): Observable<Contractor[]> {
    return this.http.get(this.url + 'contractors', {
      params: new HttpParams()
        .set('size', pageSize.toString())
        .set('page', pageNumber.toString())
        .set('s', filter)
    }).pipe(map((res: Contractor[]) => res));
  }

  add(contractor: Contractor): Observable<Contractor> {
    return this.http.post(this.url + 'contractors', contractor)
      .pipe(map((res: Contractor) => res));
  }

  update(contractor: Contractor): Observable<Contractor> {
    return this.http.put(this.url + 'contractors', contractor)
      .pipe(map((res: Contractor) => res));
  }

  getById(id: number): Observable<Contractor> {
    return this.http.get(this.url + 'contractors/' + id)
      .pipe(map((res: Contractor) => res));
  }

  delete(id: number): Observable<void> {
    return this.http.delete(this.url + 'contractors/' + id)
      .pipe(map(() => {
      }));
  }

}
