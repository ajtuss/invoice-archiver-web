import {Component, OnInit} from '@angular/core';
import {FormBuilder, ValidationErrors, Validators} from '@angular/forms';
import {ContractorService} from '../contractor.service';
import {Contractor} from '../contractors-datasource';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-edit-contractor',
  templateUrl: './edit-contractor.component.html',
  styleUrls: ['./edit-contractor.component.css'],
})
export class EditContractorComponent implements OnInit{
  contractorForm = this.fb.group({
    id: [],
    shortName: [null, Validators.required],
    fullName: [null, Validators.required],
    city: [null, Validators.required],
    zip: [null, Validators.compose([
      Validators.required, Validators.minLength(5), Validators.maxLength(5)])
    ],
    street: [null, Validators.required],
    email: [null, Validators.required],
    nip: [null, Validators.required]
  });

  constructor(private contractorService: ContractorService, private fb: FormBuilder, private route: ActivatedRoute) {}


  onSubmit() {
    this.contractorService.update(<Contractor>this.contractorForm.value).subscribe(
      value => console.log(value),
      error => {
        error.error.errors.forEach(e => this.setError(e.field, {[e.defaultMessage]: true})
        );
      },
      () => alert('Contractor is saved'));
  }


  setError(fieldName: string, errors?: ValidationErrors) {
    this.contractorForm.controls[fieldName].setErrors(errors);
  }

  ngOnInit(): void {
    this.route.params.subscribe(value => {
      this.contractorService.getById(+value['id']).subscribe(
        value1 => this.contractorForm.setValue(value1),
        error1 => console.log(error1)
      );
    });
  }
}
