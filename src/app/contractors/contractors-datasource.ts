import {CollectionViewer, DataSource} from '@angular/cdk/collections';
import {MatPaginator, MatSort} from '@angular/material';
import {catchError, finalize} from 'rxjs/operators';
import {BehaviorSubject, Observable, of} from 'rxjs';
import {ContractorService} from './contractor.service';

export interface Contractor {
  id: number;
  shortName: string;
  fullName: string;
  city: string;
  zip: string;
  street: string;
  email: string;
  nip: string;
}

/**
 * Data source for the Contractors view. This class should
 * encapsulate all logic for fetching and manipulating the displayed data
 * (including sorting, pagination, and filtering).
 */
export class ContractorsDataSource extends DataSource<Contractor> {

  private contractorSubject = new BehaviorSubject<Contractor[]>([]);
  private loadingSubject = new BehaviorSubject<boolean>(false);

  constructor(private contractorService: ContractorService, private paginator: MatPaginator, private sort: MatSort) {
    super();
  }

  /**
   * Connect this data source to the table. The table will only update when
   * the returned stream emits new items.
   * @returns A stream of the items to be rendered.
   */
  connect(collectionViewer: CollectionViewer): Observable<Contractor[]> {
    return this.contractorSubject.asObservable();
  }

  /**
   *  Called when the table is being destroyed. Use this function, to clean up
   * any open connections or free any held resources that were set up during connect.
   */
  disconnect() {
    this.contractorSubject.complete();
    this.loadingSubject.complete();
  }

  loadContractors(filter = '',
                sortDirection = 'asc', pageIndex = 0, pageSize = 10) {

    this.loadingSubject.next(true);

    this.contractorService.getAll(pageIndex, pageSize).pipe(
      catchError(() => of([])),
      finalize(() => this.loadingSubject.next(false))
    )
      .subscribe(contractor => this.contractorSubject.next(contractor));
  }

}
