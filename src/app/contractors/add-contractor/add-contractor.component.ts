import {Component} from '@angular/core';
import {FormBuilder, ValidationErrors, Validators} from '@angular/forms';
import {ContractorService} from '../contractor.service';
import {Contractor} from '../contractors-datasource';
import {Router} from '@angular/router';

@Component({
  selector: 'app-add-contractor',
  templateUrl: './add-contractor.component.html',
  styleUrls: ['./add-contractor.component.css'],
})
export class AddContractorComponent {
  contractorForm = this.fb.group({
    shortName: [null, Validators.required],
    fullName: [null, Validators.required],
    city: [null, Validators.required],
    zip: [null, Validators.compose([
      Validators.required, Validators.minLength(5), Validators.maxLength(5)])
    ],
    street: [null, Validators.required],
    email: [null, Validators.required],
    nip: [null, Validators.required]
  });

  constructor(private contractorService: ContractorService,
              private fb: FormBuilder,
              private router: Router) {}

  onSubmit() {
    this.contractorService.add(<Contractor>this.contractorForm.value).subscribe(
      value => this.router.navigate(['contractors', value.id]),
      error => {
        error.error.errors.forEach(e => this.setError(e.field, {[e.defaultMessage]: true})
        );
      },
      () => alert('Contractor is added'));
  }

  setError(fieldName: string, errors?: ValidationErrors) {
    this.contractorForm.controls[fieldName].setErrors(errors);
  }
}
