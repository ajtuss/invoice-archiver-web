import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatSort} from '@angular/material';
import {Contractor, ContractorsDataSource} from './contractors-datasource';
import {SelectionModel} from '@angular/cdk/collections';
import {ContractorService} from './contractor.service';

@Component({
  selector: 'app-contractors',
  templateUrl: './contractors.component.html',
  styleUrls: ['./contractors.component.css'],
})
export class ContractorsComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource: ContractorsDataSource;
  selection = new SelectionModel<Contractor>(false, []);


  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['select', 'id', 'shortName', 'fullName', 'city', 'zip', 'email', 'nip'];


  constructor(private contractorService: ContractorService) {
  }

  ngOnInit() {
    this.dataSource = new ContractorsDataSource(this.contractorService, this.paginator, this.sort);
    this.dataSource.loadContractors();
  }

  deleteSelectedContractor() {
    const id = this.selection.selected[0].id;
    this.contractorService.delete(id).subscribe(
      () => {},
      error1 => console.log(error1),
      () => {
        alert('Contractor is deleted.');
        this.dataSource.loadContractors();
      }
    );
  }
}
