import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {LayoutModule} from '@angular/cdk/layout';
import {
  MatAutocompleteModule,
  MatButtonModule,
  MatCardModule,
  MatCheckboxModule,
  MatDatepickerModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatPaginatorModule,
  MatRadioModule,
  MatSelectModule,
  MatSidenavModule,
  MatSortModule,
  MatTableModule,
  MatToolbarModule
} from '@angular/material';
import {MainNavComponent} from './main-nav/main-nav.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {UsersComponent} from './users/users.component';
import {InvoicesComponent} from './invoices/invoices.component';
import {ContractorsComponent} from './contractors/contractors.component';
import {HomeComponent} from './home/home.component';
import {CompanyComponent} from './company/company.component';
import {HttpClientModule} from '@angular/common/http';
import {AddCompanyComponent} from './company/add-company/add-company.component';
import {ReactiveFormsModule} from '@angular/forms';
import {EditCompanyComponent} from './company/edit-company/edit-company.component';
import {AddContractorComponent} from './contractors/add-contractor/add-contractor.component';
import {EditContractorComponent} from './contractors/edit-contractor/edit-contractor.component';
import {RelatedContractorsComponent} from './company/related-contractors/related-contractors.component';
import {AddInvoiceComponent} from './invoices/add-invoice/add-invoice.component';
import {EditInvoiceComponent} from './invoices/edit-invoice/edit-invoice.component';
import {MAT_MOMENT_DATE_ADAPTER_OPTIONS, MatMomentDateModule} from '@angular/material-moment-adapter';

@NgModule({
  declarations: [
    AppComponent,
    MainNavComponent,
    UsersComponent,
    InvoicesComponent,
    ContractorsComponent,
    HomeComponent,
    CompanyComponent,
    AddCompanyComponent,
    EditCompanyComponent,
    AddContractorComponent,
    EditContractorComponent,
    RelatedContractorsComponent,
    AddInvoiceComponent,
    EditInvoiceComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    LayoutModule,
    HttpClientModule,
    MatToolbarModule,
    MatDatepickerModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    BrowserAnimationsModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatInputModule,
    MatSelectModule,
    MatRadioModule,
    MatCardModule,
    MatCheckboxModule,
    MatAutocompleteModule,
    ReactiveFormsModule,
    // MatNativeDateModule,
    MatMomentDateModule
  ],
  providers: [
    { provide: MAT_MOMENT_DATE_ADAPTER_OPTIONS, useValue: { useUtc: true } }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
