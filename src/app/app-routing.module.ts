import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from './home/home.component';
import {UsersComponent} from './users/users.component';
import {InvoicesComponent} from './invoices/invoices.component';
import {ContractorsComponent} from './contractors/contractors.component';
import {CompanyComponent} from './company/company.component';
import {AddCompanyComponent} from './company/add-company/add-company.component';
import {EditCompanyComponent} from './company/edit-company/edit-company.component';
import {AddContractorComponent} from './contractors/add-contractor/add-contractor.component';
import {EditContractorComponent} from './contractors/edit-contractor/edit-contractor.component';
import {RelatedContractorsComponent} from './company/related-contractors/related-contractors.component';
import {AddInvoiceComponent} from './invoices/add-invoice/add-invoice.component';
import {EditInvoiceComponent} from './invoices/edit-invoice/edit-invoice.component';

const routes: Routes = [
  // { path: '', redirectTo: '/validations', pathMatch: 'full'},
  {path: '', component: HomeComponent},
  {path: 'users', component: UsersComponent},
  {path: 'invoices', component: InvoicesComponent},
  {path: 'invoices/add', component: AddInvoiceComponent},
  {path: 'invoices/:id', component: EditInvoiceComponent},
  {path: 'companies', component: CompanyComponent},
  {path: 'companies/add', component: AddCompanyComponent},
  {path: 'companies/:id', component: EditCompanyComponent},
  {path: 'companies/:id/contractors', component: RelatedContractorsComponent},
  {path: 'contractors', component: ContractorsComponent},
  {path: 'contractors/add', component: AddContractorComponent},
  {path: 'contractors/:id', component: EditContractorComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
