import {Component, OnInit} from '@angular/core';
import {FormBuilder, ValidationErrors, Validators} from '@angular/forms';
import {CompanyService} from '../company.service';
import {Company} from '../company-datasource';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-edit-company',
  templateUrl: './edit-company.component.html',
  styleUrls: ['./edit-company.component.css'],
})
export class EditCompanyComponent implements OnInit {
  companyForm = this.fb.group({
    id: [],
    shortName: [null, Validators.required],
    fullName: [null, Validators.required],
    city: [null, Validators.required],
    zip: [null, Validators.compose([
      Validators.required, Validators.minLength(5), Validators.maxLength(5)])
    ],
    street: [null, Validators.required],
    email: [null, Validators.required],
    nip: [null, Validators.required]
  });

  constructor(private companyService: CompanyService, private fb: FormBuilder, private route: ActivatedRoute) {
  }

  onSubmit() {
    this.companyService.update(<Company>this.companyForm.value).subscribe(
      () => {},
      error => {
        error.error.errors.forEach(e => this.setError(e.field, {
          [e.defaultMessage]: true
        }));
      },
      () => alert('Company is saved'));
  }

  setError(fieldName: string, errors?: ValidationErrors) {
    this.companyForm.controls[fieldName].setErrors(errors);
  }

  ngOnInit(): void {
    this.route.params.subscribe(value => {
      this.companyService.getById(+value['id']).subscribe(
        value1 => this.companyForm.setValue(value1)
      );
    });
  }
}
