import {CollectionViewer, DataSource} from '@angular/cdk/collections';
import {MatPaginator, MatSort} from '@angular/material';
import {catchError, finalize} from 'rxjs/operators';
import {BehaviorSubject, Observable, of} from 'rxjs';
import {CompanyService} from './company.service';

export interface Company {
  id: number;
  shortName: string;
  fullName: string;
  city: string;
  zip: string;
  street: string;
  email: string;
  nip: string;
}

/**
 * Data source for the Customers view. This class should
 * encapsulate all logic for fetching and manipulating the displayed data
 * (including sorting, pagination, and filtering).
 */
export class CompanyDatasource extends DataSource<Company> {

  private companySubject = new BehaviorSubject<Company[]>([]);
  private loadingSubject = new BehaviorSubject<boolean>(false);

  public loading$ = this.loadingSubject.asObservable();

  constructor(private companyService: CompanyService, private paginator: MatPaginator, private sort: MatSort) {
    super();
  }

  /**
   * Connect this data source to the table. The table will only update when
   * the returned stream emits new items.
   * @returns A stream of the items to be rendered.
   */
  connect(collectionViewer: CollectionViewer): Observable<Company[]> {
    return this.companySubject.asObservable();
  }

  /**
   *  Called when the table is being destroyed. Use this function, to clean up
   * any open connections or free any held resources that were set up during connect.
   */
  disconnect(collectionViewer: CollectionViewer): void {
    this.companySubject.complete();
    this.loadingSubject.complete();
  }

  loadCompanies(filter = '',
                sortDirection = 'asc', pageIndex = 0, pageSize = 10) {

    this.loadingSubject.next(true);

    this.companyService.getAll(pageIndex, pageSize).pipe(
      catchError(() => of([])),
      finalize(() => this.loadingSubject.next(false))
    )
      .subscribe(company => this.companySubject.next(company));
  }

}

