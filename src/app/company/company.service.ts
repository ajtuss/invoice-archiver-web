import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Company} from './company-datasource';
import {map} from 'rxjs/operators';
import {Contractor} from '../contractors/contractors-datasource';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CompanyService {

  private url: string = environment.apiURL;

  constructor(private http: HttpClient) {
  }

  getAll(pageNumer: number, pageSize: number, filter = ''): Observable<Company[]> {
    return this.http.get(this.url + 'companies', {
      params: new HttpParams()
        .set('size', pageSize.toString())
        .set('page', pageNumer.toString())
        .set('s', filter)
    }).pipe(map((res: Company[]) => res));
  }

  add(company: Company): Observable<Company> {
    return this.http.post(this.url + 'companies', company)
      .pipe(map((res: Company) => res));
  }

  update(company: Company): Observable<Company> {
    return this.http.put(this.url + 'companies', company)
      .pipe(map((res: Company) => res));
  }

  getById(id: number): Observable<Company> {
    return this.http.get(this.url + 'companies/' + id)
      .pipe(map((res: Company) => res));
  }

  delete(id: number): Observable<void> {
    return this.http.delete(this.url + 'companies/' + id)
      .pipe(map(() => {
      }));
  }

  getAllContractorsByCompanyId(companyId: number, pageSize: number, filter = ''): Observable<Contractor[]> {
    return this.http.get(this.url + 'companies/' + companyId + '/contractors', {
      params: new HttpParams()
        .set('s', filter)
    })
      .pipe(map((res: Contractor[]) => res));
  }

  addContractorToCompany(companyId: number, contractorShortName: number): Observable<void> {
    return this.http.post(this.url + 'companies/' + companyId + '/contractors', {'shortName': contractorShortName})
      .pipe(map(() => {
      }));
  }

  deleteContractorFromCompany(companyId: number, contractorId: number): Observable<void> {
    return this.http.delete(this.url + 'companies/' + companyId + '/contractors/' + contractorId).pipe(map(() => {
    }));
  }
}
