import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatSort} from '@angular/material';
import {RelatedContractorsDataSource} from './related-contractors-datasource';
import {SelectionModel} from '@angular/cdk/collections';
import {Contractor} from '../../contractors/contractors-datasource';
import {ActivatedRoute} from '@angular/router';
import {CompanyService} from '../company.service';
import {Company} from '../company-datasource';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {debounceTime, startWith, switchMap} from 'rxjs/operators';
import {ContractorService} from '../../contractors/contractor.service';


@Component({
  selector: 'app-related-contractors',
  templateUrl: './related-contractors.component.html',
  styleUrls: ['./related-contractors.component.css'],
})
export class RelatedContractorsComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource: RelatedContractorsDataSource;
  selection = new SelectionModel<Contractor>(false, []);
  company: Company;

  contractorSelect = new FormControl();
  filteredOptions: Observable<Company[]>;


  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['select', 'id', 'shortName', 'fullName', 'city', 'zip', 'email', 'nip'];

  constructor(private companyService: CompanyService, private contractorService: ContractorService, private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.dataSource = new RelatedContractorsDataSource(this.companyService, this.paginator, this.sort);
    this.route.params.subscribe(value => {
      const id = +value['id'];
      this.companyService.getById(id).subscribe(
        value1 => this.company = value1,
        error1 => console.log(error1),
      );
      this.dataSource.loadContractors(id);
    });
    this.filteredOptions = this.contractorSelect.valueChanges
      .pipe(
        startWith(''),
        debounceTime(200),
        switchMap(value => this.contractorService.getAll(0, 10, value))
      );
  }

  deleteSelectedContractorFromCompany() {
    const id = this.selection.selected[0].id;
    this.companyService.deleteContractorFromCompany(this.company.id, id).subscribe(
      () => {
      },
      error1 => console.log(error1),
      () => {
        alert('Contractor is deleted.');
        this.dataSource.loadContractors(this.company.id);
      }
    );
  }

  addSelectedContractorToCompany() {
    this.companyService.addContractorToCompany(this.company.id, this.contractorSelect.value).subscribe(
      () => {
      },
      error1 => console.log(error1),
      () => {
        this.dataSource.loadContractors(this.company.id);
        this.contractorSelect.setValue('');
      }
    );
  }
}
