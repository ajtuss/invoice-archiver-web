import {CollectionViewer, DataSource} from '@angular/cdk/collections';
import {MatPaginator, MatSort} from '@angular/material';
import {BehaviorSubject, Observable, of} from 'rxjs';
import {Contractor} from '../../contractors/contractors-datasource';
import {CompanyService} from '../company.service';
import {catchError, finalize} from 'rxjs/operators';


export class RelatedContractorsDataSource extends DataSource<Contractor> {
  private contractorSubject = new BehaviorSubject<Contractor[]>([]);
  private loadingSubject = new BehaviorSubject<boolean>(false);

  constructor(private companyService: CompanyService, private paginator: MatPaginator, private sort: MatSort) {
    super();
  }

  connect(collectionViewer: CollectionViewer): Observable<Contractor[]> {
    return this.contractorSubject.asObservable();
  }

  disconnect() {
    this.contractorSubject.complete();
    this.loadingSubject.complete();
  }

  loadContractors(companyId: number, filter = '',
                  sortDirection = 'asc', pageIndex = 0, pageSize = 10) {
    this.companyService.getAllContractorsByCompanyId(companyId, pageSize).pipe(
      catchError(() => of([])),
      finalize(() => this.loadingSubject.next(false))
    )
      .subscribe(contractor => this.contractorSubject.next(contractor));
  }
}

