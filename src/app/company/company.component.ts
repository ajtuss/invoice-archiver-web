import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatSort} from '@angular/material';
import {Company, CompanyDatasource} from './company-datasource';
import {CompanyService} from './company.service';
import {SelectionModel} from '@angular/cdk/collections';

@Component({
  selector: 'app-customers',
  templateUrl: './company.component.html',
  styleUrls: ['./company.component.css'],
})
export class CompanyComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource: CompanyDatasource;
  selection = new SelectionModel<Company>(false, []);


  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['select', 'id', 'shortName', 'fullName', 'city', 'zip', 'email', 'nip'];


  constructor(private companyService: CompanyService) {
  }

  ngOnInit() {
    this.dataSource = new CompanyDatasource(this.companyService, this.paginator, this.sort);
    this.dataSource.loadCompanies();
  }

  deleteSelectedCompany() {
    const id = this.selection.selected[0].id;
    this.companyService.delete(id).subscribe(
      () => {},
      error1 => console.log(error1),
      () => {
        alert('Company is deleted.');
        this.dataSource.loadCompanies();
      }
      );
  }
}
