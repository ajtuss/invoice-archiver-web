import {Component} from '@angular/core';
import {FormBuilder, ValidationErrors, Validators} from '@angular/forms';
import {Company} from '../company-datasource';
import {CompanyService} from '../company.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-add-company',
  templateUrl: './add-company.component.html',
  styleUrls: ['./add-company.component.css'],
})
export class AddCompanyComponent {
  companyForm = this.fb.group({
    shortName: [null, Validators.required],
    fullName: [null, Validators.required],
    city: [null, Validators.required],
    zip: [null, Validators.compose([
      Validators.required, Validators.minLength(5), Validators.maxLength(5)])
    ],
    street: [null, Validators.required],
    email: [null, Validators.required],
    nip: [null, Validators.required]
  });

  constructor(private companyService: CompanyService,
              private fb: FormBuilder,
              private router: Router) {
  }

  onSubmit() {
    this.companyService.add(<Company>this.companyForm.value).subscribe(
      value => this.router.navigate(['companies', value.id]),
      error => {
        error.error.errors.forEach(e => this.setError(e.field, {
          [e.defaultMessage]: true
        }));
      },
      () => alert('Company is added'));
  }

  setError(fieldName: string, errors?: ValidationErrors) {
    this.companyForm.controls[fieldName].setErrors(errors);
  }
}
